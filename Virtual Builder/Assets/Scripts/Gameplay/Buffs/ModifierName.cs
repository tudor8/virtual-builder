﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A list of all possible stat/modifier names
/// </summary>
public enum ModifierName {
	Damage,
	FireRate,
	Accuracy,
	Velocity,
	MagazineSize,
	StrikeEfficiency,
	StoneEfficiency, 
	WoodEfficiency,
	Health
}
