﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// The types modifiers can have.
/// </summary>
public enum ModifierType {
	Base,
	Increased,
	More
}
