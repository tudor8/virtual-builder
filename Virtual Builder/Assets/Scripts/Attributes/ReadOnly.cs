﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A readonly field can not be edited but can still be viewed in the inspector.
/// </summary>
public class ReadOnly : PropertyAttribute {}